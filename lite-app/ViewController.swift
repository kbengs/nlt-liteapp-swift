//
//  ViewController.swift
//  lite-app
//
//  Created by Kari on 10-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import Reachability

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView : WKWebView!
    let toolbar = UIToolbar()
    let progressBar = UIProgressView(progressViewStyle: .bar)
    var homeUrl: String?
    var url: String?
    var tipUrl: String?
    let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String ?? "0"
    let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
    
    internal var animationDuration = TimeInterval(0.5)
    internal var popupText = "Ingen internetanslutning"
    internal var popupBackgroundColor = UIColor.red
    internal var popupTextColor = UIColor.white
    
    let reachability = Reachability()!
    var label : UILabel?
    
    convenience init(url: String) {
        self.init(nibName: nil, bundle: nil)
        self.url = url
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let bundle = Bundle.main
        if let info = bundle.infoDictionary {
            if let hurl = info["HomeURL"] as? String {
                self.homeUrl = hurl
            }
            
            if let turl = info["TipURL"] as? String {
                self.tipUrl = turl
            }
            
        }
    }
    
    override func loadView() {
        super.loadView()
        self.webView = WKWebView()
        self.webView.navigationDelegate = self
        self.view = self.webView
    }
    
    /*override func viewDidAppear(_ animated: Bool) {
     super.viewDidAppear(animated)
     getLocation()
     }*/
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        weak var wSelf = self
        
        reachability.whenReachable = { reachability in
            if let sSelf = wSelf {
                DispatchQueue.main.async {
                    if sSelf.label != nil {
                        UIView.animate(withDuration: self.animationDuration,
                                       animations: { () -> Void in
                                        sSelf.label!.alpha = 0
                                        sSelf.label = nil
                        })
                    }
                }
            }
        }
        
        reachability.whenUnreachable = { reachability in
            if let sSelf = wSelf {
                DispatchQueue.main.async {
                    sSelf.showNoConnectionMessage()
                }
            }
        }
        
        try! reachability.startNotifier()
        
        if !reachability.isReachable {
            showNoConnectionMessage()
        }
    }
    
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        let originalUserAgent = defaults.string(forKey: "UserAgent")
        
        let userContentController = WKUserContentController()
        if #available(iOS 11.0, *) {
            let httpCookieStore = WKWebsiteDataStore.default().httpCookieStore
            httpCookieStore.getAllCookies { (cookies) in
                let script = self.getJSCookiesString(cookies: cookies)
                DispatchQueue.main.async {
                    let cookieScript = WKUserScript(source: script, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
                    userContentController.addUserScript(cookieScript)
                }
            }
            
        }
        else {
            if let cookies = HTTPCookieStorage.shared.cookies {
                let script = getJSCookiesString(cookies: cookies)
                DispatchQueue.main.async {
                    let cookieScript = WKUserScript(source: script, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: false)
                    userContentController.addUserScript(cookieScript)
                }
            }
        }
        
        let config = WKWebViewConfiguration()
        //config.userContentController = userContentController
        config.processPool = ProcessPool.sharedInstance
        /*if #available(iOS 10.0, *) {
         config.mediaTypesRequiringUserActionForPlayback = []
         } else {
         config.requiresUserActionForMediaPlayback = false
         }*/
        config.allowsInlineMediaPlayback = true
        //config.applicationNameForUserAgent = appName.folding(options: .diacriticInsensitive, locale: .current) + " iOS Version/\(appVersion)"
        webView = WKWebView(frame: .zero, configuration: config)
        //webView = WKWebView(frame: CGRect(x: 0, y: 22, width: view.frame.width, height: view.frame.height-66), configuration: config)

        webView.customUserAgent = originalUserAgent! + " " + appName + " iOS " + (appVersion)
        
        webView.scrollView.isScrollEnabled = true               // Make sure our view is interactable
        webView.scrollView.bounces = false                    // Things like this should be handled in web code
        webView.allowsBackForwardNavigationGestures = false   // Disable swiping to navigate
        webView.contentMode = .scaleToFill                    // Scale the page to fill the web view
        
        //webView.frame = CGRect(x: 0, y: 22, width: view.frame.width, height: view.frame.height-66)
        //webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.navigationDelegate = self
        
        //toolbar.frame = CGRect(x: 0, y: view.frame.height-44, width: view.frame.width, height: 44)
        //toolbar.autoresizingMask = [.flexibleTopMargin , .flexibleRightMargin , .flexibleWidth]
        
        //progressBar.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.size.height, width: view.frame.width, height: 2)
        progressBar.alpha = 0
        progressBar.tintColor = UIColor.init(red: 0.7765, green: 0.0588, blue: 0.0745, alpha: 1)
        //progressBar.autoresizingMask = .flexibleWidth
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        //var backIcon = UIBarButtonItem(barButtonSystemIt .done, target: self, action: "closeWebview")
        
        let backIcon = UIBarButtonItem(image: UIImage(named: "icon-back"), style: .plain, target: self , action: #selector(goBackTapped))
        backIcon.tintColor = UIColor.black
        
        let homeIcon = UIBarButtonItem(image: UIImage(named: "icon-home"), style: .plain, target: self , action: #selector(homeTapped))
        homeIcon.tintColor = UIColor.black
        
        let refreshIcon = UIBarButtonItem(image: UIImage(named: "icon-refresh"), style: .plain, target: self , action: #selector(refreshTapped))
        refreshIcon.tintColor = UIColor.black
        
        let tipIcon = UIBarButtonItem(image: UIImage(named: "icon-tip"), style: .plain, target: self , action: #selector(tipTapped))
        tipIcon.tintColor = UIColor.black
        
        let settingsIcon = UIBarButtonItem(image: UIImage(named: "icon-settings"), style: .plain, target: self , action: #selector(settingsTapped))
        settingsIcon.tintColor = UIColor.black
        
        toolbar.setItems([flex,backIcon,flex,homeIcon,flex,refreshIcon,flex,tipIcon,flex,settingsIcon,flex], animated: true)
        toolbar.barTintColor = UIColor.white
        toolbar.barStyle = UIBarStyle.black
        
        let notificationName = Notification.Name("updateWebViewUrl")
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.updateWebViewUrl), name: notificationName, object: nil)
        
        if let url = self.url, let nurl = URL(string: url) {
            loadRequest(NSMutableURLRequest(url: nurl))
            //webView.load(URLRequest(url: nurl))
        }
        else {
            //webView.load(URLRequest(url: URL(string: homeUrl!)!))
            loadRequest(NSMutableURLRequest(url: URL(string: homeUrl!)!))
        }
        
        webView.uiDelegate = self
        
        view.addSubview(progressBar)
        view.addSubview(webView)
        view.addSubview(toolbar)
        view.backgroundColor = UIColor.white
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        
        if #available(iOS 11, *) {
            let guide = view.safeAreaLayoutGuide
            
            let progressTop = progressBar.topAnchor.constraint(equalTo: guide.topAnchor, constant: 0.0)
            let progressLeading = progressBar.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0.0)
            let progressTrailing = progressBar.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0.0)
            let webViewTop = webView.topAnchor.constraint(equalTo: progressBar.bottomAnchor, constant: 0.0)
            let webViewLeading = webView.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0.0)
            
            let webViewTrailing = webView.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0.0)
            let webViewHeight = webView.bottomAnchor.constraint(equalTo: toolbar.topAnchor, constant: 0.0)
            
            let toolbarTrailing = toolbar.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: 0.0)
            let toolbarBottom = toolbar.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: 0.0)
            let toolbarHeight = toolbar.heightAnchor.constraint(equalToConstant: 44)
            let toolbarLeading = toolbar.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 0.0)
            
            let defaultConstraints = [progressTop, progressLeading, progressTrailing, webViewTop, webViewLeading, toolbarHeight, toolbarBottom, toolbarTrailing, webViewHeight, webViewTrailing, toolbarLeading]
            view.addConstraints(defaultConstraints)
        } else {
            let progressTop = progressBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 0.0)
            let progressLeading = progressBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0)
            let progressTrailing = progressBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0)
            let webViewTop = webView.topAnchor.constraint(equalTo: progressBar.bottomAnchor, constant: 0.0)
            let webViewLeading = webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0)
            
            let webViewTrailing = webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0)
            let webViewHeight = webView.bottomAnchor.constraint(equalTo: toolbar.topAnchor, constant: 0.0)
            
            let toolbarTrailing = toolbar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0)
            let toolbarBottom = toolbar.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: 0.0)
            let toolbarHeight = toolbar.heightAnchor.constraint(equalToConstant: 44)
            let toolbarLeading = toolbar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0)
            
            let defaultConstraints = [progressTop, progressLeading, progressTrailing, webViewTop, webViewLeading, toolbarHeight, toolbarBottom, toolbarTrailing, webViewHeight, webViewTrailing, toolbarLeading]
            
            view.addConstraints(defaultConstraints)
            
        }
        
        TSMobileAnalytics.sendTag(withCategories: ["native"], contentName: "", contentID: "") { (success, error) in
            if let tError = error {
                // Handle error.
                print("Error: \(tError.localizedDescription)")
            }
        }
        //print (UserDefaults.standard.dictionaryRepresentation())
        
    }
    
    open func loadRequest(_ request: NSMutableURLRequest?) {
        /*if let url = request?.url, let cookies = HTTPCookieStorage.shared.cookies(for: url) {
         request?.allHTTPHeaderFields = HTTPCookie.requestHeaderFields(with: cookies)
         }*/
        if let request = request {
            webView.load(request as URLRequest)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //self.clearCache()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {

        super.viewWillDisappear(animated)
        
        reachability.stopNotifier()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressBar.alpha = 1.0
            progressBar.setProgress(Float(webView.estimatedProgress), animated: true)
            if(self.webView.estimatedProgress >= 1.0) {
                UIView.animate(withDuration: 0.3 , delay: 0.1, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in self.progressBar.alpha = 0.0}, completion: {(finished:Bool) -> Void in self.progressBar.progress = 0})
            }
            
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //self.progressBar.setProgress(0.0, animated: false)
        webView.evaluateJavaScript("javascript:if(typeof window.setIsApp == 'function') { window.setIsApp(); };") { (result, error) -> Void in }
    }
    
    // To dealt with JS window open or target = _blank tag
    // - result : open Safari app
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if UIApplication.shared.canOpenURL(navigationAction.request.url!) {
            UIApplication.shared.openURL(navigationAction.request.url!)
        }
        
        return nil
    }
    
    
    /*func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let urlResponse = navigationResponse.response as? HTTPURLResponse,
            let url = urlResponse.url,
            let allHeaderFields = urlResponse.allHeaderFields as? [String : String] {
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: allHeaderFields, for: url)
       
            HTTPCookieStorage.shared.setCookies(cookies , for: urlResponse.url!, mainDocumentURL: nil)
            decisionHandler(.allow)
        }
    }*/
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let url = navigationAction.request.url?.absoluteString
        let hostAddress = navigationAction.request.url?.host
        
        /*if hostAddress == URL(string: homeUrl!)!.host {
            webView.configuration.applicationNameForUserAgent = appName.folding(options: .diacriticInsensitive, locale: .current) + " iOS Version/\(appVersion)"
        }
        else {
            webView.configuration.applicationNameForUserAgent = ""
        }*/
        // To connnect app store
        if hostAddress == "itunes.apple.com" {
            if UIApplication.shared.canOpenURL(navigationAction.request.url!) {
                UIApplication.shared.openURL(navigationAction.request.url!)
                decisionHandler(.cancel)
                return
            }
        }
        
        let url_elements = url!.components(separatedBy: ":")
        
        switch url_elements[0] {
            
        case "tel":
            openCustomApp(urlScheme: "telprompt://", additional_info: url_elements[1])
            decisionHandler(.cancel)
            
        case "sms":
            openCustomApp(urlScheme: "sms://", additional_info: url_elements[1])
            decisionHandler(.cancel)
            
        case "mailto":
            openCustomApp(urlScheme: "mailto://", additional_info: url_elements[1])
            decisionHandler(.cancel)
            
        default:
            break
        }
        
        decisionHandler(.allow)
    }
    
    /**
     open custom app with urlScheme : telprompt, sms, mailto
     
     - parameter urlScheme: telpromt, sms, mailto
     - parameter additional_info: additional info related to urlScheme
     */
    func openCustomApp(urlScheme:String, additional_info:String){ 
        if let requestUrl:NSURL = NSURL(string:"\(urlScheme)"+"\(additional_info)") {
            let application:UIApplication = UIApplication.shared
            if application.canOpenURL(requestUrl as URL) {
                application.openURL(requestUrl as URL)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        reachability.stopNotifier()
    }
    
    func settingsTapped(sender: UIBarButtonItem) {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settings") as! SettingsViewController
        
        self.present(viewController, animated: true, completion: nil)
    }
    
    func goBackTapped(sender: UIBarButtonItem) {
        if (self.webView.canGoBack) {
            self.webView.goBack()
        }
        /*else {
         self.webView.load(URLRequest(url: URL(string: homeUrl!)!))
         }*/
    }
    
    func refreshTapped(sender: UIBarButtonItem) {
        self.webView.reloadFromOrigin()
    }
    
    func homeTapped(sender: UIBarButtonItem) {
        //self.webView.load(URLRequest(url: URL(string: homeUrl!)!))
        loadRequest(NSMutableURLRequest(url: URL(string: homeUrl!)!))
    }
    
    func tipTapped(sender: UIBarButtonItem) {
        //self.webView.load(URLRequest(url: URL(string: tipUrl!)!))
        loadRequest(NSMutableURLRequest(url: URL(string: tipUrl!)!))
    }
    
    func updateWebViewUrl() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let loadURL = appDelegate.loadUrl
        
        guard let url = URL(string: loadURL!) else {
            print("Invalid URL")
            return
        }
        
        //let request = URLRequest(url: url)
        //self.webView.load(request)
        loadRequest(NSMutableURLRequest(url: url))
    }
    
    /*func shareTapped(sender: UIBarButtonItem) {
        
        if self.webView.url != nil {
            let shareTitle: String = self.webView.title!
            let shareUrl: URL = self.webView.url!
            
            let activityViewController = UIActivityViewController(activityItems: [shareTitle,shareUrl], applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            activityViewController.popoverPresentationController?.barButtonItem = sender
            
            // exclude everything but sms, mail, facebook and twitter
            if #available(iOS 9.0, *) {
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,  UIActivityType.assignToContact,  UIActivityType.openInIBooks, UIActivityType.print, UIActivityType.postToFlickr,  UIActivityType.postToTencentWeibo,  UIActivityType.postToVimeo,  UIActivityType.postToWeibo,  UIActivityType.saveToCameraRoll ]
            } else {
                activityViewController.excludedActivityTypes = [ UIActivityType.airDrop,  UIActivityType.assignToContact, UIActivityType.print, UIActivityType.postToFlickr,  UIActivityType.postToTencentWeibo,  UIActivityType.postToVimeo,  UIActivityType.postToWeibo,  UIActivityType.saveToCameraRoll ]
                
            }
            OperationQueue.main.addOperation {
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }*/
    
    /*static func loadUrl(url: String) {
        if !url.isEmpty {
            let presentedvc = getTopViewController()
            
            if presentedvc is SettingsViewController {
                presentedvc?.dismiss(animated: true, completion: {() -> Void in
                    let vc = getTopViewController() as! ViewController
                    vc.webView.load(URLRequest(url: URL(string: url)!))
                })
            }
            else {
                let vc = getTopViewController() as! ViewController
                vc.webView.load(URLRequest(url: URL(string: url)!))
            }
        }
    }*/
    
    static func getTopViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController
        {
            while (topController.presentedViewController != nil)
            {
                topController = topController.presentedViewController!
            }
            return topController
        }
        return nil
    }
    
    func showNoConnectionMessage() {
        
        if label != nil {
            return
        }
        
        let bounds = view.bounds
        label = UILabel(frame: CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: 66))
        label!.text = popupText
        label!.backgroundColor = popupBackgroundColor
        label!.textColor = popupTextColor
        
        label!.alpha = 0
        label!.textAlignment = .center
        
        view.addSubview(label!)
        
        label!.translatesAutoresizingMaskIntoConstraints = false
        let topGuide = self.topLayoutGuide
        let viewsDictionary = ["label": label!, "topGuide": topGuide] as [String: AnyObject]
        
        let constraintsV = NSLayoutConstraint.constraints(withVisualFormat: "V:[topGuide]-0-[label(10@20)]",
                                                          options: NSLayoutFormatOptions(rawValue: 0),
                                                          metrics: nil,
                                                          views: viewsDictionary)
        
        let constraintsH = NSLayoutConstraint.constraints(withVisualFormat: "|-0-[label]-0-|",
                                                          options: NSLayoutFormatOptions(rawValue: 0),
                                                          metrics: nil,
                                                          views: viewsDictionary)
        
        self.view.addConstraints(constraintsV + constraintsH)
        
        UIView.animate(withDuration: animationDuration,
                       animations: { () -> Void in
                        self.label!.alpha = 1
        })
    }
    
    ///Generates script to create given cookies
    public func getJSCookiesString(cookies: [HTTPCookie]) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"
        
        for cookie in cookies {
            result += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            if let date = cookie.expiresDate {
                result += "expires=\(dateFormatter.string(from: date)); "
            }
            if (cookie.isSecure) {
                result += "secure; "
            }
            result += "'; "
        }
        return result
    }
    
}

