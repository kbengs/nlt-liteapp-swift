//
//  SettingsSwitchCell.swift
//  lite-app
//
//  Created by Kari on 17-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit

class SettingsSwitchCell: UITableViewCell {
    
    @IBOutlet weak var enablePush: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    }
    
}
