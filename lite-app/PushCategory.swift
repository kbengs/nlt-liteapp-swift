//
//  PushCategory.swift
//  lite-app
//
//  Created by Kari on 16-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import Foundation

struct PushCategory {
    let label: String
    let tag: String
    let selected: Bool
}

extension PushCategory {
    
    /// Load all the elements from the plist file
    static func loadFromPlist() -> [PushCategory] {
        // First we need to find the plist
        let file = Bundle.main.path(forResource: "PushCategories", ofType: "plist")
        
        // Then we read it as an array of dict
        let array = NSArray(contentsOfFile: file!) as? [[String: AnyObject]]
        
        // Initialize the array
        var categories: [PushCategory] = []
        
        // For each dictionary
        for dict in array! {
            let category = PushCategory.from(dict: dict)
            categories.append(category)
        }

        return categories
    }
    
    static func from(dict: [String: AnyObject]) -> PushCategory {
        let label = dict["label"] as! String
        let tag = dict["tag"] as! String
        let selected = dict["selected"] as! Bool
        
        return PushCategory(label: label, tag: tag, selected: selected)
    }
}
