//
//  SettingsViewController.swift
//  lite-app
//
//  Created by Kari on 15-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit
import OneSignal
import WebKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet var tableView: UITableView!
    
    var enablePush: UISwitch!
    
    var pushCategories: [PushCategory] = []
       
    override func viewDidLoad() {
        super.viewDidLoad()

        pushCategories = PushCategory.loadFromPlist()
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        OneSignalUserID = status.subscriptionStatus.userId
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refreshForm()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Refresh the notification settings when we come back in the app, since the user can have changed them in iOS' settings
        NotificationCenter.default.addObserver(self, selector: #selector(refreshForm), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        // Also refresh the notification settings when iOS tells us they changed, since DidBecomeActiveNotification arrives a little too early
        NotificationCenter.default.addObserver(self, selector: #selector(refreshForm), name: NSNotification.Name(rawValue: UserNotificationChanged), object: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //
        NotificationCenter.default.removeObserver(self)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return pushCategories.count
        }
        else if section == 2 {
            return 2
        }
        else {
            return 1
        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if (section == 0) {
            return "Pushinställningar"
        }
        else if (section == 1) {
            return "Avdelningar"
        }
        else if (section == 2) {
            return "Allmänna inställningar"
        }
        else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        if (indexPath as NSIndexPath).section == 2 && (indexPath as NSIndexPath).row == 1 {
            return true
        }
        else {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
         return action == #selector(copy(_:))
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        if action == #selector(copy(_:)) {
            let cell = tableView.cellForRow(at: indexPath)
            let pasteboard = UIPasteboard.general
            pasteboard.string = cell?.detailTextLabel?.text
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if (indexPath as NSIndexPath).section == 0 {
            //if (indexPath as NSIndexPath).row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "enablePushCell") as! SettingsSwitchCell
            if let notifSettings = UIApplication.shared.currentUserNotificationSettings {
                cell.enablePush.isOn = notifSettings.types.rawValue != 0
            } else {
                cell.enablePush.isOn = false
            }

            self.enablePush = cell.enablePush
                return cell
            //}
        }
        else if (indexPath as NSIndexPath).section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
            cell.notificationLabel.text = pushCategories[indexPath.row].label
            cell.update(pushCategories[indexPath.row])
            if !self.enablePush.isOn {
                cell.notificationEnabledSwitch.isEnabled = false
            }
            return cell
        }
        else if (indexPath as NSIndexPath).section == 2 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "debugCell", for: indexPath)
                if indexPath.row == 0 {
                    let versionNumberString = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                    cell.textLabel?.text = "Version"
                    cell.detailTextLabel?.text = versionNumberString
                }
                else if indexPath.row == 1 {
                    cell.textLabel?.text = "Id"
                    cell.detailTextLabel?.text = OneSignalUserID
                    //print (OneSignalUserID)
                    //cell.cellLabel.text = "Id"
                    //cell.cellValue.text = BatchInstallationID
                }
                return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "deleteDataCell", for: indexPath) as! ButtonCell
            cell.tapAction = { (cell) in
                self.clearCache()
            }
            return cell
        }

    }
    
    func clearCache() {
        let alert = UIAlertController(title: "Rensa", message: "Vill du rensa appens dokument och data?", preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Rensa", style: UIAlertActionStyle.destructive, handler: { action in
            
            if #available(iOS 9.0, *) {
                let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
                let date = NSDate(timeIntervalSince1970: 0)
                WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler:{ })
            }
            else
            {
                URLCache.shared.removeAllCachedResponses()
                // Delete system cookie store in the app
                let storage = HTTPCookieStorage.shared
                if let cookies = storage.cookies {
                    for cookie in cookies {
                        storage.deleteCookie(cookie)
                    }
                }
            }
            
            
        }))
        alert.addAction(UIAlertAction(title: "Avbryt", style: UIAlertActionStyle.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }

    func refreshForm() {
        tableView.reloadData()
    }
    
    @IBAction func enablePushToggled(_ sender: Any) {
        
        // If we're sure we never asked for the OS settings, show the notification optin modal
        /*if !PushManager().didTriggerSystemPopup {
            self.enablePush.setOn(!self.enablePush.isOn, animated: true)
            
             PushManager().register()
            
            return
        }*/
        
        let alert = UIAlertController(title: "Notiser",
                                      message: "Notisinställningar för denna app ändras under Inställningar.",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Visa", style: .default) { (_) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        
        alert.addAction(UIAlertAction(title: "Avbryt", style: .cancel) { (_) in
            self.enablePush.setOn(!self.enablePush.isOn, animated: true)
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
