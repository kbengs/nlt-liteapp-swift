//
//  AppDelegate.swift
//  lite-app
//
//  Created by Kari on 10-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit
import OneSignal


var OneSignalAppID = ""
let UserNotificationChanged = "userNotificationSettingsChanged"
var OneSignalUserID = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {
    
    var window: UIWindow?
    var loadUrl: String?
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let defaults = UserDefaults.standard
        assert(Thread.current.isMainThread, "This method must be called on the main thread.")
        let webView = UIWebView()
        let userAgent = webView.stringByEvaluatingJavaScript(from: "navigator.userAgent")!
        
        defaults.set(userAgent, forKey: "UserAgent")
        return true
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        //OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            
            if let url = payload.additionalData["url"] as? String {
                self.loadUrl = url
                let notificationName = Notification.Name("updateWebViewUrl")
                NotificationCenter.default.post(name: notificationName, object: nil)
                //ViewController.loadUrl(url: url)
            }
            
        }
        
        /*let playerId = UserDefaults.standard.object(forKey: "OneSignal.Player.Id") as? String ?? ""
        if !playerId.isEmpty {
                UserDefaults.standard.set(playerId, forKey: "GT_PLAYER_ID")
                UserDefaults.standard.removeObject(forKey: "OneSignal.Player.Id")
            
        }*/
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true]
        
        let bundle = Bundle.main
        if let info = bundle.infoDictionary,
            let oneSignalAppId = info["OneSignalAppID"] as? String {
            if !oneSignalAppId.isEmpty {
                OneSignal.initWithLaunchOptions(launchOptions,
                                                appId: oneSignalAppId,
                                                handleNotificationAction: notificationOpenedBlock,
                                                settings: onesignalInitSettings)
                
                OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification

            }
            else {
                print ("OneSignalAppID is missing")
            }
        }
    
        let subscriptionManager = SubscriptionManager()
        if !subscriptionManager.didSyncWithOneSignal {
            subscriptionManager.initDefaults()
        }
        else {
            subscriptionManager.getTags()
        }
        
        //OneSignal.add(self as OSPermissionObserver)
        OneSignal.add(self as OSSubscriptionObserver)
        
        /*for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
            print("\(key) = \(value) \n")
        }*/
        
        TSMobileAnalytics.createInstance(withCPID: "ce3341cf-e9a9-4a41-9801-27ab6ecdc06e", applicationName: "NLT", trackPanelist: true, keychainAccessGroup: nil)
        //TSMobileAnalytics.setLogPrintsActivated(true)
        
        return true
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            //print("Subscribed for OneSignal push notifications!")
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            OneSignalUserID = status.subscriptionStatus.userId
        }
        //print("SubscriptionStateChange: \n\(stateChanges)")
    }
    
    /*func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                //print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                //print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        //print("PermissionStateChanges: \n\(stateChanges)")
    }*/

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //ViewController.clearCache()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    }
    
    /*@available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
           //if sourceApplication == "se.tns-sifo.panelen" {
            return TSMobileTagging.getInstance().application(
                app,
                open: url as URL!,
                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplicationOpenURLOptionsKey.annotation]
            )
        //}

    }*/

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return TSMobileAnalytics.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: UserNotificationChanged), object: nil)
    }
    
    /*func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let url = userInfo["url"] as? String {
            ViewController.loadUrl(url: url)
        }
        
        completionHandler(UIBackgroundFetchResult.noData)
    }*/

}

