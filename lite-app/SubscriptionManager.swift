//
//  SubscriptionManager.swift
//  lite-app
//
//  Created by Kari on 20-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import Foundation
import OneSignal
import UIKit

private let pushCategories = "push_categories"

class SubscriptionManager {
    
    var didSyncWithOneSignal: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "syncWithOneSignal")
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "syncWithOneSignal")
        }
    }
    
    func isSubscribedToCategory(_ name: String) -> Bool {
        if let subscribedCategories = UserDefaults.standard.stringArray(forKey: pushCategories) {
            return subscribedCategories.index(of: name) != nil
        }
        
        return false
    }
    
    func togglePushCategory(_ name: String, enabled: Bool) {
        toggleArrayValue(pushCategories, value: name, enabled: enabled)
    }
    
    fileprivate func toggleArrayValue(_ arrayName: String, value: String, enabled: Bool) {
        let defaults = UserDefaults.standard
        var array: [String] = defaults.stringArray(forKey: arrayName) ?? []
        
        if enabled {
            if array.index(of: value) == nil {
                array.append(value)
            }
        } else {
            array = array.filter({ (arrayItem) -> Bool in
                arrayItem != value
            })
        }
        
        defaults.setValue(array, forKey: arrayName)
        
        syncWithOneSignal()
    }
    
    func getTags() {
        let defaults = UserDefaults.standard
        var array: [String] = defaults.stringArray(forKey: pushCategories) ?? []
        OneSignal.getTags({ tags in
            //print("tags - \(tags!)")
            //user has tags set these in pushCategories
            tags?.forEach({(tag) in
                array.append(tag.key as! String)
            })
            defaults.setValue(array, forKey: pushCategories)
        }, onFailure: { error in
            print("Error getting tags - \(error?.localizedDescription ?? "")")
        })

    }
    
    // Sets the default subscriptions
    func initDefaults() {
        let defaults = UserDefaults.standard
        var array: [String] = defaults.stringArray(forKey: pushCategories) ?? []
        let categories = PushCategory.loadFromPlist()
        
        categories.forEach({ (category) in
            let enabled = UserDefaults.standard.object(forKey: "push_" + category.tag) as? Bool ?? category.selected
            //print ("checking %s", "push_" + category.tag)
            if enabled {
                if array.index(of: category.tag) == nil {
                    array.append(category.tag)
                }
            }
        })
        defaults.setValue(array, forKey: pushCategories)
        if let notifSettings = UIApplication.shared.currentUserNotificationSettings {
            if notifSettings.types.rawValue != 0 {
                OneSignal.setSubscription(true)
            }
        }
        syncWithOneSignal()
        didSyncWithOneSignal = true
        //print("did init");
        
    }
    
    // Synchronise data in NSUserDefaults with One Signal
    func syncWithOneSignal() {
        let defaults = UserDefaults.standard
        
        var tags: [AnyHashable:Any] = [:]
        let categories = PushCategory.loadFromPlist()
        
        let array: [String] = defaults.stringArray(forKey: pushCategories)!
        
        categories.forEach({ (category) in
            if array.index(of: category.tag) != nil {
                tags[category.tag] = category.tag
            }
            else {
                tags[category.tag] = ""
            }
        })
        
        OneSignal.sendTags(tags, onSuccess: { result in
            print("Tags sent - \(result!)")
        }) { error in
            print("Error sending tags: \(error?.localizedDescription ?? "")")
        }
    
    }
    
    
}
