//
//  ButtonCell.swift
//  lite-app
//
//  Created by Kari on 19-06-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    
    var tapAction: ((UITableViewCell) -> Void)?
    
    @IBAction func buttonTap(_ sender: Any) {
        tapAction?(self)
    }
    
}
